Criteo OneTag Extension
=====================
An Magento extension to implement Criteo One Tag with built in feed generator
Facts
-----
- version: 1.6.0
- extension key: Criteo_OneTag

Description
-----------
An Magento extension to implement Criteo One Tag with built in feed generator

Requirements
------------
- PHP >= 5.2.0
- Mage_Core
- ...

Compatibility
-------------
- Magento >= 1.9

Licence
-------
[OSL - Open Software Licence 3.0](http://opensource.org/licenses/osl-3.0.php)

Copyright
---------
(c) 2017 Criteo
