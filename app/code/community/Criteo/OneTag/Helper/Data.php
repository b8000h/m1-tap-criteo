<?php

class Criteo_OneTag_Helper_Data extends Mage_Core_Helper_Abstract {

    const SETTINGS_PARTNER_ID = 'Criteo_OneTag/settings/partner_id';
    const SETTINGS_PRODUCT_ID = 'Criteo_OneTag/settings/product_id';
    const SETTINGS_TAG_HOMEPAGE_ACTIVATED = 'Criteo_OneTag/settings/tag_homepage_activated';
    const SETTINGS_TAG_LISTING_ACTIVATED = 'Criteo_OneTag/settings/tag_listing_activated';
    const SETTINGS_TAG_PRODUCT_ACTIVATED = 'Criteo_OneTag/settings/tag_product_activated';
    const SETTINGS_TAG_BASKET_ACTIVATED = 'Criteo_OneTag/settings/tag_basket_activated';
    const SETTINGS_TAG_SALES_ACTIVATED = 'Criteo_OneTag/settings/tag_sales_activated';
    const FEED_SETTINGS_FEED_ACTIVATED = 'Criteo_OneTag/feed_settings/feed_activated';
    const FEED_SETTINGS_PASSWORD = 'Criteo_OneTag/feed_settings/password';
    const FEED_SETTINGS_PRODUCT_TYPE = 'Criteo_OneTag/feed_settings/product_type';
    const FEED_SETTINGS_PRODUCT_PER_PAGE = 'Criteo_OneTag/feed_settings/product_per_page';

    public function getPartnerId($store = null) {
        return Mage::getStoreConfig(self::SETTINGS_PARTNER_ID);
    }

    public function getUseSku($store = null) {
        return Mage::getStoreConfig(self::SETTINGS_PRODUCT_ID);
    }

    public function getHomepageActivated($store = null) {
        return Mage::getStoreConfig(self::SETTINGS_TAG_HOMEPAGE_ACTIVATED);
    }

    public function getListingActivated($store = null) {
        return Mage::getStoreConfig(self::SETTINGS_TAG_LISTING_ACTIVATED);
    }

    public function getProductActivated($store = null) {
        return Mage::getStoreConfig(self::SETTINGS_TAG_PRODUCT_ACTIVATED);
    }

    public function getCartActivated($store = null) {
        return Mage::getStoreConfig(self::SETTINGS_TAG_BASKET_ACTIVATED);
    }

    public function getSaleActivated($store = null) {
        return Mage::getStoreConfig(self::SETTINGS_TAG_SALES_ACTIVATED);
    }

    public function getFeedActivated($store = null) {
        return Mage::getStoreConfig(self::FEED_SETTINGS_FEED_ACTIVATED);
    }

    public function getFeedPassword($store = null) {
        return Mage::getStoreConfig(self::FEED_SETTINGS_PASSWORD);
    }

    public function getProductTypes($store = null) {
        $productTypes = Mage::getStoreConfig(self::FEED_SETTINGS_PRODUCT_TYPE);
        return explode(",", $productTypes);
    }

    public function getProductPerPage($store = null) {
        $productPerPage = (int) Mage::getStoreConfig(self::FEED_SETTINGS_PRODUCT_PER_PAGE);
        if (!$productPerPage) {
            $productPerPage = 200; //default if not maintained
        }
        return $productPerPage;
    }

}
