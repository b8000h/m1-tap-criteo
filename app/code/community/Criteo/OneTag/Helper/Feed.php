<?php

class Criteo_OneTag_Helper_Feed extends Mage_Core_Helper_Abstract {

    private $feedData;
    private $BASE_URL;
    private $BASE_URL_IMAGE;

    public function __construct() {
        $this->feedData = array();
        $this->BASE_URL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
        $this->BASE_URL_IMAGE = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
    }

    public function generateFeed($page = 1) {

        $pageSize = Mage::helper('Criteo_OneTag')->getProductPerPage();
        $productTypes = Mage::helper('Criteo_OneTag')->getProductTypes();

        if ($page) {
            //get maximum feed page and check if we reach the last page
            $maxPage = $this->getMaximumFeedPage($pageSize, $productTypes);
            if ($page > $maxPage) {
                http_response_code(404);
                die();
            }
        }

        $collection = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect('*')
                ->addFieldToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
                ->addFieldToFilter('type_id', array('in' => $productTypes));

        if ($page) {
            $collection->setPageSize($pageSize)->setCurPage($page);
        }

        //iterate through resource and create feed
        foreach ($collection as $item) {
            $product = new stdClass();
            try {

                $product->id = $item->getEntityId();
                $product->type_id = $item->getTypeId();

                //skip simple products that have parent
                if ($product->type_id == 'simple') {
                    $parentIds = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($product->id);
                    if (!empty($parentIds)) {
                        continue;
                    }
                }

                $product->title = $this->addCDATA($item->getName());
                $product->description = $this->addCDATA($item->getShortDescription());
                $product->sku = $item->getSku();
                $product->link = $this->BASE_URL . $item->getUrlPath();

                //$product->image_link = Mage::helper('catalog/image')->init($item, 'image');
                $image_link = $item->getImage();
                if (empty($image_link)) {
                    $image_link = $item->getSmallImage();
                }
                $product->image_link = $this->BASE_URL_IMAGE . 'catalog/product' . $image_link;

                
                $product->base_price = (float) $item->getPrice();
                $product->special_price = (float) $item->getSpecialPrice();
                $product->final_price = (float) $item->getFinalPrice();
                $product->min_price = (float) $item->getMinimalPrice();
                
                // arg 2，关掉 format 避免产生逗号和货币符号
                // arg 3，避免 HTML tag
                $product->base_price = Mage::helper('core')->currency($product->base_price,false,false);
                $product->special_price = Mage::helper('core')->currency($product->special_price,false,false);
                $product->final_price = Mage::helper('core')->currency($product->final_price,false,false);
                $product->min_price = Mage::helper('core')->currency($product->min_price,false,false);
                
                // 此处切勿用 number_format( )，这货会加入逗号
                $product->base_price = (float) round($product->base_price,2);
                $product->special_price = (float) round($product->special_price,2);
                $product->final_price = (float) round($product->final_price,2);
                $product->min_price = (float) round($product->min_price,2);
                
                $product->availability = $item->isAvailable() ? 'in stock' : 'out of stock';

                $cats = $item->getCategoryIds();
                if (isset($cats)) {
                    $product->product_type = implode(' > ', array_slice($cats, -3, 3));
                }

                array_push($this->feedData, $product);
            } catch (Exception $exc) {
                //is there anything we can do?
            }
        }

        //output XML to browser
        $xml = $this->generateXmlFeed($this->feedData);
        header('Content-type: text/xml; charset=utf-8');
        echo($xml);
    }

    /*
     * Get last feed page = total products / product per page
     */

    private function getMaximumFeedPage($pageSize = 200,
            $productTypes = array(Mage_Catalog_Model_Product_Type::TYPE_SIMPLE)) {

        if (!$pageSize) {
            return 0;
        }

        $maxPage = 0;

        $dbResource = Mage::getSingleton('core/resource');
        $dbConnection = $dbResource->getConnection('core_read');

        $productTypesQuery = "'" . implode("', '", $productTypes) . "'";
        $productTableName = $dbResource->getTableName('catalog/product');
        $testQuery = <<<EOT
            SELECT COUNT(*) AS crtoCount FROM $productTableName WHERE type_id IN ($productTypesQuery);
EOT;

        $results = $dbConnection->fetchAll($testQuery);
        if (isset($results[0])) {
            $count = (int) $results[0]['crtoCount'];
            $maxPage = round(($count / $pageSize), 0, PHP_ROUND_HALF_UP);
        }
        return $maxPage;
    }

    private function generateXmlFeed($feed_data) {
        $xml_string = '<rss version="2.0" xmlns:g="http://base.google.com/ns/1.0">' . PHP_EOL;
        foreach ($feed_data as $row) {
            $xml_string .= self::makeXmlRow($row) . PHP_EOL;
        }
        $xml_string .= '</rss>';
        return $xml_string;
    }

    private function makeXmlRow($row) {
        $xml_string = '<item>' . PHP_EOL;
        foreach ($row as $key => $value) {
            $xml_string .= '<g:' . $key . '>';
            $xml_string .= $value;
            $xml_string .= '</g:' . $key . '>';
        }
        $xml_string .= '</item>' . PHP_EOL;
        return $xml_string;
    }

    private function addCDATA($string) {
        return '<![CDATA[' . $string . ']]>';
    }

}
