<?php

class Criteo_OneTag_Model_Source_ProductType {

    public function toOptionArray() {
        return array(
            array('value' => Mage_Catalog_Model_Product_Type::TYPE_SIMPLE, 'label' => 'Simple'),
            array('value' => Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE, 'label' => 'Configurable'),
            array('value' => Mage_Catalog_Model_Product_Type::TYPE_BUNDLE, 'label' => 'Bundle'),
            array('value' => Mage_Catalog_Model_Product_Type::TYPE_GROUPED, 'label' => 'Grouped'),
            array('value' => Mage_Catalog_Model_Product_Type::TYPE_VIRTUAL, 'label' => 'Virtual'),
            array('value' => Mage_Downloadable_Model_Product_Type::TYPE_DOWNLOADABLE, 'label' => 'Downloadable')
        );
    }

}
