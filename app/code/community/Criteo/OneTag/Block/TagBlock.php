<?php

class Criteo_OneTag_Block_TagBlock extends Mage_Core_Block_Template {

    public function __construct(array $args = array()) {
        parent::__construct($args);
    }

    public function generateTags() {

        $tagGenerator = Mage::helper('Criteo_OneTag/TagGenerator');
        $output = "";

        //get configured info
        $partnerId = Mage::helper('Criteo_OneTag')->getPartnerId();
        $enableHome = Mage::helper('Criteo_OneTag')->getHomepageActivated();
        $enableListing = Mage::helper('Criteo_OneTag')->getListingActivated();
        $enableProduct = Mage::helper('Criteo_OneTag')->getProductActivated();
        $enableCart = Mage::helper('Criteo_OneTag')->getCartActivated();
        $enableSale = Mage::helper('Criteo_OneTag')->getSaleActivated();
        $useSKU = Mage::helper('Criteo_OneTag')->getUseSku();

        //get info for all tags
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        if ($customer) {
            $email = $customer->getEmail();
        }

        $email = $email ? md5(strtolower(trim($email))) : '';

        //set basic tag info
        $tagGenerator->setAccount(array('account' => $partnerId));
        $tagGenerator->setEmail(array('email' => $email));

        if ($this->isHome()) {
            $tagGenerator->viewHome();
            if ($enableHome) {
                $output .= $tagGenerator->getTag();
            }
        } else if ($this->isListing()) {
            $tagGenerator->viewList(array('item' => $this->getListingIds($useSKU)));
            if ($enableListing) {
                $output .= $tagGenerator->getTag();
            }
        } else if ($this->isProduct()) {
            $tagGenerator->viewItem(array('item' => $this->getCurrentProductId($useSKU)));
            if ($enableProduct) {
                $output .= $tagGenerator->getTag();
            }
        } else if ($this->isCart()) {
            $tagGenerator->viewBasket($this->getCartData($useSKU));
            if ($enableCart) {
                $output .= $tagGenerator->getTag();
            }
        } else if ($this->isSale()) {
            $transactionData = $this->getTransactionData($useSKU);
            $tagGenerator->trackTransaction($transactionData);
            
            if ($enableSale && !empty($transactionData)) {
                $output .= $tagGenerator->getTag();
            }
        } else {
            return;
        }

        //show data layer regardless of configuration
        $output .= $tagGenerator->getDataLayer();

        return $output;
    }

    private function isHome() {
        return Mage::app()->getRequest()->getRequestString() == "/";
    }

    private function isListing() {
        return Mage::app()->getFrontController()->getRequest()->getControllerName() == "category";
    }

    private function isProduct() {
        return Mage::registry("current_product") != null;
    }

    private function isCart() {
        return Mage::app()->getFrontController()->getAction()->getFullActionName() == "checkout_cart_index";
    }

    private function isSale() {
        return Mage::app()->getFrontController()->getAction()->getFullActionName() == "checkout_onepage_success";
    }

    private function getCurrentProductId($useSKU) {
        $productId = '';
        $product = Mage::registry('current_product');
        if ($product) {
            //get parent id if any
            $product = $this->getParentProductIfAny($product);
            $productId = $product->getId();
            if ($useSKU) {
                $productId = $product->getData('sku');
            }
        }
        return $productId;
    }

    private function getListingIds($useSKU) {
        $topThreeProductIds = array();

        $blockList = $this->getLayout()->getBlock('product_list');
        if (empty($blockList)) {
            return $topThreeProductIds;
        }
        $productCollection = $blockList->getLoadedProductCollection();

        if ($productCollection) {
            foreach ($productCollection as $product) {
                $product = $this->getParentProductIfAny($product);
                $productId = $product->getId();
                if ($useSKU) {
                    $productId = $product->getData('sku');
                }
                $topThreeProductIds[] = $productId;

                if (sizeof($topThreeProductIds) == 3) {
                    break;
                }
            }
        }

        return $topThreeProductIds;
    }

    private function getCartData($useSKU) {
        $cartData = array();

        try {
            $cart = Mage::getModel('checkout/cart');
            $quote = $cart->getQuote();
            //fetch all item info 
            $items = $quote->getAllVisibleItems();
            foreach ($items as $item) {
                $product = $item->getProduct();
                $cartData[] = array(
                    'id' => ($useSKU ? $product->getData('sku') : $product->getId()),
                    'quantity' => (float) $item->getQty(),
                    'price' => (float) $product->getFinalPrice()
                );
            }
        } catch (Exception $exc) {
            //do nothing
        }
        return array('currency' => Mage::app()->getStore()->getCurrentCurrencyCode(), 'item' => $cartData);
    }

    private function getTransactionData($useSKU) {
        $transactionId = '';
        $cartData = array();

        try {
            $checkoutSession = Mage::getSingleton('checkout/session');
            $orderId = $checkoutSession->getLastOrderId();
            $quote = Mage::getModel('sales/order')->load($orderId);
            $transactionId = $quote->getIncrementId();

            if (empty($transactionId)) {
                return null;
            }

            //fetch all item info 
            $items = $quote->getAllVisibleItems();
            foreach ($items as $item) {
                $product = $item->getProduct();
                $price = (float) $item->getPrice();
                if (empty($price)) {
                    $price = (float) $product->getFinalPrice();
                }

                $cartData[] = array(
                    'id' => ($useSKU ? $product->getData('sku') : $product->getId()),
                    'quantity' => (float) $item->getQtyOrdered(),
                    'price' => $price
                );
            }
        } catch (Exception $exc) {
            //do nothing
        }

        return array(
            'id' => $transactionId,
            'currency' => Mage::app()->getStore()->getCurrentCurrencyCode(),
            'item' => $cartData);
    }

    /*
     * Get parent product if any, else return the product object itself
     */

    private function getParentProductIfAny($product) {
        if (!$product) {
            return;
        }

        /**
         * 不明白 Criteo 在此要 parent product, 这会导致 product view 页和 cart 页的 item id 不一致
         * 查明之前先注释掉
         */
        /*
        //use product parent ID if any
        $productId = $product->getId();
        $productType = $product->getTypeId();

        if ($productType == 'simple') {
            $parentIds = Mage::getModel('catalog/product_type_grouped')->getParentIdsByChild($productId);
            if (!$parentIds) {
                $parentIds = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($productId);
            }
            if (isset($parentIds[0])) {
                $parent = Mage::getModel('catalog/product')->load($parentIds[0]);
            }
        }

        if (isset($parent)) {
            return $parent;
        }
        */
        return $product;
    }

}
