<?php

class Criteo_OneTag_IndexController extends Mage_Core_Controller_Front_Action {

    private $PLUGIN_VERSION = '1.6.2.1';

    public function indexAction() {
        $debugMode = filter_input(INPUT_GET, 'debug', FILTER_SANITIZE_STRING);
        $feedPassword = (string) filter_input(INPUT_GET, 'pwd', FILTER_SANITIZE_STRING);
        $page = (int) filter_input(INPUT_GET, 'page', FILTER_SANITIZE_STRING);
        $feedActivated = Mage::helper('Criteo_OneTag')->getFeedActivated();
        $configFeedPassword = (string) Mage::helper('Criteo_OneTag')->getFeedPassword();

        if ($debugMode) {
            $this->showDebug();
            exit();
        }

        if (!$feedActivated) {
            die('Feed is not activated');
        }

        if ($configFeedPassword != $feedPassword) {
            die('Incorrect feed password given');
        }

        //start generate feed with given page
        Mage::helper('Criteo_OneTag/Feed')->generateFeed($page);
    }

    public function showDebug() {
        $partnerId = Mage::helper('Criteo_OneTag')->getPartnerId();
        $enableHome = Mage::helper('Criteo_OneTag')->getHomepageActivated();
        $enableListing = Mage::helper('Criteo_OneTag')->getListingActivated();
        $enableProduct = Mage::helper('Criteo_OneTag')->getProductActivated();
        $enableCart = Mage::helper('Criteo_OneTag')->getCartActivated();
        $enableSale = Mage::helper('Criteo_OneTag')->getSaleActivated();
        $useSKU = Mage::helper('Criteo_OneTag')->getUseSku();
        $feedActivated = Mage::helper('Criteo_OneTag')->getFeedActivated();
        $feedPassword = Mage::helper('Criteo_OneTag')->getFeedPassword();
        $productPerPage = Mage::helper('Criteo_OneTag')->getProductPerPage();
        $productTypes = implode(' , ', Mage::helper('Criteo_OneTag')->getProductTypes());

        $html = <<<EOT
            <h3>CRITEO MODULE DEBUG <i>(module version $this->PLUGIN_VERSION )</i></h3>
            <b>TAG RELATED PARAMS</b><br/>
            Account ID = <b>$partnerId</b><br/>
            Use SKU for Product ID? = <b>$useSKU</b><br/>
            Tag Homepage activated = <b>$enableHome</b><br/>
            Tag Listing activated = <b>$enableListing</b><br/>
            Tag Product activated = <b>$enableProduct</b><br/>
            Tag Basket activated = <b>$enableCart</b><br/>
            Tag Sales activated = <b>$enableSale</b><br/>
            <b>FEED RELATED PARAMS</b><br>    
            Feed activated = <b>$feedActivated</b><br/>
            Feed password = <b>$feedPassword</b><br/>  
            Number of products per feed page = <b>$productPerPage</b><br/> 
            Product types to be retrieved = <b>$productTypes</b><br/> 
EOT;
        echo $html;
    }

}
